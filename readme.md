## KeepReading - Android

Preparos

    - Vá no Console Google e ative a API GCM para Android
    - Crie um app web, e pegue a server key
    - Crie um app Android, e configure o app com os dados

No app:

    - Rode o app no dispositivo e pegue o ID de registro gerado.

Na linha de comando:
```
     curl --header "Authorization: key={server key}" --header Content-Type:"application/json" https://android.googleapis.com/gcm/send  -d "{\"link\":\"{url}\",\"registration_ids\":[\"{ID registro}\"]}"
```