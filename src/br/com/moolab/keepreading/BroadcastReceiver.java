package br.com.moolab.keepreading;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class BroadcastReceiver extends WakefulBroadcastReceiver {

 @Override
    public void onReceive(Context context, Intent intent) {
	 
	 	// Delega a responsabilidade de tratar o intent para o GcmIntentService
        ComponentName comp = new ComponentName(context.getPackageName(), HandleIntentService.class.getName());

        // Inicia o servico, mantem o dispositivo acordado.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
