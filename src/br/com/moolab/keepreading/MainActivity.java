package br.com.moolab.keepreading;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MainActivity extends Activity {

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;	
	public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";	
	private static final String TAG_ERROR_GCM =  "TAG_ERROR_GCM";
	
	/**
     * Project Number que voce obtem no console do Google Cloud
     */
    String SENDER_ID = "1006745653949";

    /**
     * Log de menssagem
     */
    static final String TAG = "TAG_GCM_MSG";

    TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;

    String regid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		context = getApplicationContext();
		
		mDisplay = (TextView) findViewById(R.id.display);
		mDisplay.setTypeface(Fonts.getInstance().getRobotoCondensed(getAssets()));

	    // Verificar se o servicos do GPS esta ativo.
	    if (checkPlayServices()) {
	    	
	       /*
	        * Se tudo ocorreu bem.
	        */	    	
	    	gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            /*
             * Se nao existe um ID de registro, solicitamos o registro.
             * Em background! 
             */
            if (regid.isEmpty()) {
                registerInBackground();
            } else {
            	mDisplay.setText(regid);
            }
            
	    } else {
	    	Log.i(TAG, "No valid Google Play Services APK found.");
	    }
	}
	
	/**
	 * Verificamos se o GPS APK esta disponivel no onResume tambem.
	 */
	@Override
	protected void onResume() {
	    super.onResume();
	    checkPlayServices();
	}

	/**
	 * Verificamos se o dispositivo tem o GPS APK. Se nao tiver abriremos um dialog para que o 
	 * usuario aceite o download do APK, o download � feito pelo Google Play Store.
	 * Ou habilitar nas configuracoes do aparelho.
	 */
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG_ERROR_GCM, "This device is not supported.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Pega o ID de registro atual do app no GCM service.
	 * 
	 * Se o resultado for empty o app precisa registrar.
	 *
	 * @return registration ID, or empty string if there is no existing registration ID.
	 */
	private String getRegistrationId(Context context) {
		
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (registrationId.isEmpty()) {
	        Log.i(TAG, "Registration not found.");
	        return "";
	    }

	    /*
	     * Verificamos se o app foi atualizado; se foi, deve limpar o ID do registro, 
	     * porque nao tem como garantir que o ID de registro vai funcionar na nova versao do app.
	     */
	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(context);
	    if (registeredVersion != currentVersion) {
	        Log.i(TAG, "App version changed.");
	        return "";
	    }
	    
	    return registrationId;
	}
	
	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
	    // Registramos no shared preferences, mas poderiamos armazenar em outro lugar.
	    return getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
	/**
	 * Registra a appp no GCM server.
	 * 
	 * Armazena o ID de registro e a versao da app nas shared preferences.
	 */
	private void registerInBackground() {
		
	    new AsyncTask<String, String, String>() {
		
	        @Override
	        protected String doInBackground(String... params) {
	            String msg = "";	       
	            
	            try {
	            	
	                if (gcm == null) {
	                    gcm = GoogleCloudMessaging.getInstance(context);
	                }
	                regid = gcm.register(SENDER_ID);
	                msg = "Device registered, registration ID=" + regid;

	                /*
	                 * Precisamos enviar o ID de registro para o nosso servidor.
	                 * Dessa maneira poderemos enviar CCS para enviar mensagens e utilizar o GCM/HTTP.
	                 * O request deveria ser autenticado se a app esta utilizando conta de usuario. 
	                 */
	                sendRegistrationIdToBackend();

	                // Guardar o ID de registro, para nao precisar registrar novamente.
	                storeRegistrationId(context, regid);
	                
	            } catch (IOException ex) {
	                msg = "Error :" + ex.getMessage();
	            }
	            return msg;
	        }

	        @Override
	        protected void onPostExecute(String msg) {
	            mDisplay.append(msg + "\n");
	        }
	        
	    }.execute(null, null, null);
	}
	    
	/**
	 * Enviar o ID de registro para o servidor de back end.
	 */	   	    
	private void sendRegistrationIdToBackend() {
		Log.i(TAG, "Enviando ID de registro: " + regid);
	}
	
	/**
	 * Armazena o ID de registro e a versao da app.
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		
	    final SharedPreferences prefs = getGCMPreferences(context);	    
	    int appVersion = getAppVersion(context);
	    Log.i(TAG, "ID de registro guardado para versao " + appVersion);
	    
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
}