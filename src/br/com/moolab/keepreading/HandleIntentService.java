package br.com.moolab.keepreading;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class HandleIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	public static final String EXTRA_LINK = "link";
	public static final String EXTRA_TITLE =  "title";
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public HandleIntentService() {
        super("HandleIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        
        //  Tipo do intent recebido no BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if ( ! extras.isEmpty()) { 
        	
            /*
             * Filtrando o tipo de mensagem, como futuramente o GCM pode criar novos tipos,
             * desconsideremos qualquer tipo que nao seja do nosso interesse.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            	
				sendNotification(extras.getString(EXTRA_TITLE), extras.getString(EXTRA_LINK));	
                
                Log.i(MainActivity.TAG, "Received: " + extras.toString());
            }
        }
        
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        BroadcastReceiver.completeWakefulIntent(intent);
    }

    // Notificando o usuario
    private void sendNotification(String title, String link) {
    	
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);        
        
        Intent intent = new Intent(this, ShowActivity.class);
        intent.putExtra(EXTRA_LINK, link);
        intent.putExtra(EXTRA_TITLE, title);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.keepreading)
        .setContentTitle(title)
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(link))        
        .setContentText(link);
        mBuilder.setAutoCancel(true);        
        
        mBuilder.setContentIntent(contentIntent);
               
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}