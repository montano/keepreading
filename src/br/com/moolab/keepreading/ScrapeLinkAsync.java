package br.com.moolab.keepreading;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.os.AsyncTask;

class ScrapeLinkAsync extends AsyncTask<String, Void, Document> {

	public interface ScrapeLinkCallback {
		public void onFinish(Document scrape);
	}

	private ScrapeLinkCallback callback;
	
	public void setCallback(ScrapeLinkCallback callback) {
		this.callback = callback;
	}
	
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Document doInBackground(String... params) {

        try {
            URL url= new URL(params[0]);
            Document doc = Jsoup.connect(url.toString()).get();
            return doc;
            
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Document result) {
        super.onPostExecute(result);
        callback.onFinish(result);
    }
}