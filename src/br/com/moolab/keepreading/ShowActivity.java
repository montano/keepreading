package br.com.moolab.keepreading;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import br.com.moolab.keepreading.ScrapeLinkAsync.ScrapeLinkCallback;

public class ShowActivity extends Activity {

	private TextView mTitle;
	private TextView mLink;
	private TextView mResume;
	private ProgressBar mLoading;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show);
		
		mTitle   = (TextView) findViewById(R.id.title);
		mTitle.setTypeface(Fonts.getInstance().getRobotoBoldCondensed(getAssets()));
		
		mResume   = (TextView) findViewById(R.id.resume);
		mResume.setTypeface(Fonts.getInstance().getRobotoLight(getAssets()));		
		
		mLink   = (TextView) findViewById(R.id.link);
		mLink.setTypeface(Fonts.getInstance().getRobotoCondensed(getAssets()));
		
		mLoading = (ProgressBar) findViewById(R.id.loading);
		
		Bundle mBundle = (savedInstanceState == null) ? getIntent().getExtras() : savedInstanceState;
		
		if (mBundle != null) {
		
			String link = mBundle.getString(HandleIntentService.EXTRA_LINK);
			
			mLink.setText(link);			
			
			// get the tables on this page, note I masked the phone number
	        ScrapeLinkAsync scrapAsync = new ScrapeLinkAsync();
	        scrapAsync.setCallback(new ScrapeLinkCallback() {
				
				@Override
				public void onFinish(Document doc) {
					
					if (doc != null) {						
						Elements materiaTitulo = doc.select(".materia-titulo");			            
						
						// get the columns
						Elements titulo1 = materiaTitulo.select("h1");
						mTitle.setText(titulo1.text());
						
						Elements tituloe2 = materiaTitulo.select("h2");
						mResume.setText(tituloe2.text());
					}
					mLoading.setVisibility(View.GONE);
					
				}
			});
	        scrapAsync.execute(link);  
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.show, menu);
		return true;
	}
}
